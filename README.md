# Pokemon Trainer

A Pokemon is a mystical creature that belongs to a fictional world, designed
and managed by the Japanese companies Nintendo, Game Freak and
Creatures. The Pokemon world is available on manga, anime adaptions, games,
retail stores and many more places

## Dependencies
To work with Pokemon it is necessary to have docker and Compose installed in versions that can work with the YAML compose configuration file in version two.

- [Docker compose file V2 reference](https://docs.docker.com/compose/compose-file/#version-2)
- [Docker Engine 20.10.3+](https://docs.docker.com/engine/installation/) - Installation guide in Linux
- [Compose 1.27.4+](https://docs.docker.com/compose/install/#/install-using-pip) - Installation guide using PIP

## Install development enviroment
* [Clone the repository](#clone-the-repository)
* [Build development enviroment](#build-development-enviroment)
* [Create network](#create-network)
* [Start development](#start-development)
* [Load SQL](#load-sql)
* [Access](#access)
* [Stop development](#stop-development)

## Work with pokemons
* [Catch a Pokemon](#catch-a-pokemon)
* [Request a pokemon from pokedex](#request-a-pokemon-from-pokedex)

### Clone the repository
```
$ cd Documents # optional
$ git clone git@gitlab.com:diegoug/pokemon-trainer.git
$ cd pokemon-trainer
```
[Back to Install development enviroment](#install-development-enviroment)

### Build development enviroment
```
$ cd pokemon-trainer
$ make build-development
```
[Back to Install development enviroment](#install-development-enviroment)

### Create network
```
$ cd pokemon-trainer
$ make create-network
```
[Back to Install development enviroment](#install-development-enviroment)

### Start development
```
$ cd pokemon-trainer
$ make start-development
```
[Back to Install development enviroment](#install-development-enviroment)

### Stop development
```
$ cd pokemon-trainer
$ make stop-development
```
[Back to Install development enviroment](#install-development-enviroment)

### Catch a Pokemon
Command
```
python3 manage.py catch <pokemon_name>
```

Docker Command
```
docker exec -it pokemon-trainer-ms-dev bash -c 'python3 manage.py catch <pokemon_name>'
```

Response
```
$ python3 manage.py catch bayleef
Blu, Blu, Blu, Click! All rigth! You caught an Bayleef!
```
[Back to Install development enviroment](#work-with-pokemons)

### Request a pokemon from pokedex
Request format
```
http://localhost/trainer/api/v1/pokedex/?name=<pokemon_name>
```
Request
```
http://localhost/trainer/api/v1/pokedex/?name=bayleef
```

Response
```json
{
    "id": 3,
    "uuid": 153,
    "name": "bayleef",
    "stats": [
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/1/",
                "name": "hp"
            },
            "effort": 0,
            "base_stat": 60
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/2/",
                "name": "attack"
            },
            "effort": 0,
            "base_stat": 62
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/3/",
                "name": "defense"
            },
            "effort": 1,
            "base_stat": 80
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/4/",
                "name": "special-attack"
            },
            "effort": 0,
            "base_stat": 63
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/5/",
                "name": "special-defense"
            },
            "effort": 1,
            "base_stat": 80
        },
        {
            "stat": {
                "url": "https://pokeapi.co/api/v2/stat/6/",
                "name": "speed"
            },
            "effort": 0,
            "base_stat": 60
        }
    ],
    "height": 12,
    "weight": 158,
    "evolution_chain": {
        "species": "chikorita",
        "evolves_to": [
            {
                "species": "bayleef",
                "evolves_to": [
                    {
                        "species": "meganium",
                        "pokemon_id": 154,
                        "species_url": "https://pokeapi.co/api/v2/pokemon-species/154/",
                        "evolution_type": "evolution"
                    }
                ],
                "pokemon_id": 153,
                "species_url": "https://pokeapi.co/api/v2/pokemon-species/153/",
                "evolution_type": "actual"
            }
        ],
        "pokemon_id": 152,
        "species_url": "https://pokeapi.co/api/v2/pokemon-species/152/",
        "evolution_type": "preevolution"
    }
}
```
[Back to Install development enviroment](#work-with-pokemons)