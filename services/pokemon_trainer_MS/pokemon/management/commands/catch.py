import requests

from django.core.management.base import BaseCommand, CommandError

from pokemon.models import Pokemon

class Command(BaseCommand):
    help = '¡Go pokemon trainer and catch one! - ' \
        'Usage: python3 manage.py catch <pokemon_name>'

    def add_arguments(self, parser):
        parser.add_argument('pokemon_name', nargs='?', type=str)

    def handle(self, *args, **options):
        pokemon_name = options['pokemon_name']

        if not pokemon_name:
            raise CommandError(
                'name a pokemon, check the help for more information')
        
        pokemon_name = pokemon_name.lower()

        if Pokemon.objects.filter(name=pokemon_name).exists():
            raise CommandError('You already caught this pokemon')

        pokemon_json = self.get_pokemon_data(pokemon_name)
        species_json = self.get_species_data(pokemon_json['species']['url'])
        evolution_json = self.get_evolution_data(
            species_json['evolution_chain']['url'])

        model_data = {
            'uuid': pokemon_json['id'],
            'name': pokemon_json['name'],
            'height': pokemon_json['height'],
            'weight': pokemon_json['weight'],
            'stats': pokemon_json['stats'],
            'evolution_chain': self.get_evolution_chain(
                pokemon_json, evolution_json['chain'])
        }

        try:
            pokemon = Pokemon(**model_data)
            pokemon.full_clean()
        except TypeError as e:
            raise CommandError('{}'.format(str(e)))
        except ValidationError as e:
            raise CommandError('{}'.format(str(e)))
        pokemon.save()
        
        self.stdout.write(self.style.SUCCESS(
            'Blu, Blu, Blu, Click! All rigth! You caught an ' \
                '{}!'.format(pokemon_name.capitalize())))
    
    def get_evolution_chain(self, actual_pokemon, chain,
                            evolution_type='preevolution'):
        
        pokemon_name = actual_pokemon['name']
        if chain['species']['name'] == pokemon_name:
            evolution_type = 'actual'
        
        pokemon_id = actual_pokemon['id']
        if evolution_type != 'actual':
            pokemon_json = self.get_pokemon_data(chain['species']['name'])
            pokemon_id = pokemon_json['id']

        evolution_chain = {
            'pokemon_id': pokemon_id,
            'evolution_type': evolution_type,
            'species': chain['species']['name'],
            'species_url': chain['species']['url'],
        }

        if True if 'evolves_to' in chain else False:
            if chain['evolves_to']:
                evolution_chain['evolves_to'] = []
                if evolution_type == 'actual':
                    evolution_type = 'evolution'
                for evolve in chain['evolves_to']:
                    if True if len(chain['evolves_to']) > 1 else False:
                        if evolve['species']['name'] != pokemon_name and \
                            not evolution_type == 'evolution':
                            evolution_type = 'parallel'
                    evolution_chain['evolves_to'].append(
                        self.get_evolution_chain(
                            actual_pokemon, evolve, evolution_type))
        return evolution_chain

    def get_pokemon_data(self, pokemon_name):
        pokemon_url = 'https://pokeapi.co/api/v2/pokemon/{}'.format(
            pokemon_name)
        response = requests.get(pokemon_url)

        if response.status_code != 200:
            raise CommandError('Pokemon "{}" {}'.format(
                pokemon_name.capitalize(), response.text))

        return response.json()
    
    def get_species_data(self, species_url):
        response = requests.get(species_url)

        if response.status_code != 200:
            raise CommandError('{}'.format(response.text))

        return response.json()
    
    def get_evolution_data(self, evolution_url):
        response = requests.get(evolution_url)

        if response.status_code != 200:
            raise CommandError('{}'.format(response.text))

        return response.json()
