
from django.urls import path

from .views import PokemonView

urlpatterns = [
    path('api/v1/pokedex/',
        PokemonView.as_view(),
        name='pokemon_get'),
]
