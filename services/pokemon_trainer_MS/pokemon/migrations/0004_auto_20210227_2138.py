# Generated by Django 3.1.7 on 2021-02-27 21:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pokemon', '0003_auto_20210227_1930'),
    ]

    operations = [
        migrations.RenameField(
            model_name='pokemon',
            old_name='evolution',
            new_name='evolution_chain',
        ),
    ]
