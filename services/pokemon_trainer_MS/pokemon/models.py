from django.db import models


class Pokemon(models.Model):
    uuid = models.PositiveIntegerField(default=0)
    name = models.CharField(max_length=30)
    stats = models.JSONField()
    height = models.PositiveIntegerField(default=0)
    weight = models.PositiveIntegerField(default=0)
    evolution_chain = models.JSONField()
