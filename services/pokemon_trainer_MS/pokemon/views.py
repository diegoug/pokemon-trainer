import json

from django.views.generic import View
from django.http import HttpResponse

from .models import Pokemon


class PokemonView(View):
    def get(self, request, *args, **kwargs):
        if 'name' in request.GET:
            name = request.GET.get('name')
        else:
            message = {'message': 'you can not request a pokemon, ' \
                      'because some parameters are missing.'}
            return HttpResponse(json.dumps(message),
                                status=403, content_type="application/json")

        pokemon_queryset = Pokemon.objects.filter(name=name)
        if not pokemon_queryset.exists():
            message = {'message': "you haven't caught this pokemon"}
            return HttpResponse(json.dumps(message),
                                status=404, content_type="application/json")
        
        pokemon = pokemon_queryset.values().first()
        
        return HttpResponse(json.dumps(pokemon),
                            content_type="application/json", status=200)
